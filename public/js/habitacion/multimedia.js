/**
 * Created by hbs on 18/08/16.
 */
var base='';
$(document).ready(function() {
    base=$("#baseUrl").val();
    var con= 0, actual=0;
    var llenar=1;
    $(function () {
        $('#fileupload').fileupload({
            dataType: 'json',
            add: function (e, data) {

                con++;
                $('.panelimagenes').append(
                    "<div class='row card-panel paneles hoverable' id='paneles"+con+"' num='"+con+"'>"+

                    "<div class='progress red'>"+
                    "<div class='determinate' style='width: 0%;' id='progressBar"+con+"'></div>"+
                    "</div>"+
                    "<div class='col m4'>"+
                    "<output class='row col m12 card-image'>"+
                    "<img class='row col m12 imgns imagen"+con+"'>"+
                    "</output>"+
                    "</div>"+
                    "<h6 class='col m8 textimage"+con+"'></h6>"+

                    "<div class='col m12 input-field'>" +
                    "<textarea id='descripcionimage"+con+"' class='materialize-textarea'></textarea>"+
                    "<label for='descripcionimage"+con+"'>Breve descripción</label>"+

                    "</div>"+
                    "<div class='row col m12'>" +
                    "<div class='col m6 btn  pink darken-1 waves-effect waves-light' id='subirimage"+con+"' con='"+con+"'><i class='material-icons right'>cloud</i>Guardar</div>"+
                    "<div class='col m6 btn eliminar grey darken-3 waves-effect waves-light' id='eliminar"+con+"' num='"+con+"'><i class='material-icons right'>delete</i>Cancelar</div>"+

                    "</div>"+
                    "</div>"
                );
                data.context = $('#subirimage'+con)
                    .click(function () {
                        data.context = $('<p/>').append("<div class='col m6 btn  pink darken-1 waves-effect waves-light' id='subirimage"+con+"'><i class='material-icons right'>cloud</i>Subiendo..</div>").replaceAll($(this));
                        data.submit();
                        actual=$(this).attr('con');
                    });
                $(".textimage"+con).append(data.files[0].name);

                //VISTA PREVIA
                var files = data.files; // FileList object
                // Obtenemos la imagen del campo "file".
                for (var i = 0, f; f = files[i]; i++) {
                    //Solo admitimos imágenes.
                    if (!f.type.match('image.*')) {
                        continue;
                    }

                    var reader = new FileReader();

                    reader.onload = (function(theFile) {
                        return function(e) {
                            // Insertamos la imagen
                            $(".imagen"+llenar).attr('src', e.target.result).attr('title', escape(theFile.name))
                            llenar++;
                        };
                    })(f);

                    reader.readAsDataURL(f);
                }

            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress'+actual).removeClass('hide');
                $('#progressBar'+actual).css(
                    'width',
                    progress + '%'
                );
            },

            done: function (e, d) {

                guardarMultimedia({o:1,id: $("#idhabitacion").val(), t:1, d:$('#descripcionimage'+con).val(), p: 'public/server/php/files/'+d.jqXHR.responseJSON.files[0].name}, $(".imagen"+actual), d.files[0].name, actual);
                d.context.empty().append("<div class='col m6 btn  pink darken-1 waves-effect waves-light disabled'><i class='material-icons right'>done_all</i>Finish!</div>");
                $("#eliminar"+actual).addClass('disabled');


            }
        });
    });
});

function guardarMultimedia(data, img, name, actual)
{
    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        data: data,
        url: base+"/habitacion/multimedia",
        success: function (data) {
            img.attr('src',base+"/"+data).attr('title', name);
            $("#bibliotecaimage").val($("#bibliotecaimage").val()+"<a class='carousel-item' href='#"+actual+"!'><img src='"+base+"/"+data+"'></a>");
            $("#idcoru").empty().append("<div class='carousel'>"+$("#bibliotecaimage").val()+"</div>");
            $(".carousel").carousel();
            Materialize.toast('Guardada correctamente!', 2000,'',function(){$("#paneles"+actual).hide();})

        }

    });
}


$(document).on('click','div.eliminar', function() {
    var num=$(this).attr('num');
    $.each($(".paneles"), function()
    {
        if($(this).attr('num')==num)
        {
            $(this).hide();
        }
    })
});

