<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Habitacion\Controller;

use Bus\Form\BusForm;
use Bus\Model\Asiento;
use Bus\Model\Bus;
use Habitacion\Form\HabitacionForm;
use Habitacion\Model\Habitacion;
use Habitacion\Model\Multimedia;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{

    public $dbAdapter;
    public $bustable;
    public $habitaciontable;
    public $clientetable;
    public $multimediatable;

    public function indexAction()
    {

        return new ViewModel(array(
            "buses",
        ));
    }
    public function multimediaAction()
    {
        $id = (int)$this->params()->fromRoute('id', 0);
        if ($this->request->isXmlHttpRequest()) {
            $o = (int)$this->request->getPost('o');
            switch ($o) {

                case 1:
                    $descripcion = $this->request->getPost('d');
                    $path = $this->request->getPost('p');
                    $tipo = (int)$this->request->getPost('t');
                    $id= (int)$this->request->getPost('id');
                    $multimedia= new Multimedia();
                    $multimedia->path=$path;
                    $multimedia->descripcion=$descripcion;
                    $multimedia->estado=1;
                    $multimedia->tipo=$tipo;
                    $multimedia->idhabitacion=$id;
                    $id=$this->getMultimediaTable()->saveMultimedia($multimedia);
                    if($id)
                        return new JsonModel(array($multimedia->path));
                    break;
            }

        }
        return new ViewModel(array(
            'id'=>$id
        ));


    }
    public function addAction()
    {

        $idlogeado=1;//TODO: CAMBIAR A ID LOGIN
        if ($this->request->isXmlHttpRequest()) {
            $o = (int)$this->request->getPost('o');
            $idpro = (int)$this->request->getPost('id');
            switch ($o) {
                case 2:
                    $unidades= $this->getUnindadTable()->getUnidadDistrito($idpro);
                    return new JsonModel($unidades);
                    break;
                case 1:
                    $unidades= $this->getDistritoTable()->getDistritoProvincia($idpro);
                    return new JsonModel($unidades);
                    break;
            }

        }

        $cliente=$this->getClienteTable()->getCliente($idlogeado);
        $form= new HabitacionForm();
        $request = $this->getRequest();

        $form->get('idpersona')->setValue($cliente->idpersona);
        if ($request->isPost()) {

            $habitacion= new Habitacion();
            $form->setInputFilter($habitacion->getInputFilter());

            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
            $form->setData($post);
            //return new JsonModel(array($form->get('idpersona')->getValue()));

            if ($form->isValid()) {
                $habitacion->exchangeArray($form->getData());
                $idhabitacion= $this->getHabitacionTable()->saveHabitacion($habitacion);
                if($idhabitacion)
                    return $this->redirect()->toRoute('habitacion', array('action'=>'multimedia', 'id'=>$idhabitacion));
            }
        }
        return new ViewModel(array('form'=>$form));
    }

    public function editAction()
    {
        $id = (int)$this->params()->fromRoute('id', 0);

        if($id)
        {
            $form= new PersonasForm();
            $person=$this->getPersonaTable()->getPersona($id);
            $foto1=$person->fotodni;
            $foto2=$person->fotopersonal;
            $unidades=$this->getUnindadTable()->getUnidadDistrito(1);
            $unidad = array();
            foreach ($unidades as $value) {
                $unidad[$value['idunidad']] = $value['nombre'];
            }
            $form->get('idunidad')->setAttribute('options', $unidad);
            $form->bind($person);
            $request = $this->getRequest();
            if ($request->isPost()) {
                $form->setInputFilter($person->getInputFilter());

                $post = array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                );
                $form->setData($post);
                if ($form->isValid()) {
                    $person->idpersona=$id;
                    $data = $form->getData();

                    $path = $data->fotodni;
                    $sinpunto=substr($path['tmp_name'], -(strlen($path['tmp_name'])-1));
                    $pathfin=str_replace("\\","/",$sinpunto);
                    $person->fotodni=$pathfin;
                    $path = $data->fotopersonal;
                    $sinpuntos=substr($path['tmp_name'], -(strlen($path['tmp_name'])-1));
                    $pathfins=str_replace("\\","/",$sinpuntos);
                    $person->fotopersonal=$pathfins;

                    if($person->fotodni==""){
                        $person->fotodni=$foto1;
                    }
                    if($person->fotopersonal=="")
                    {
                        $person->fotopersonal=$foto2;
                    }
                    $this->getPersonaTable()->savePersona($person);

                    return $this->redirect()->toRoute(
                        'persona',
                        array(
                            'action' => 'index',
                        )
                    );
                }

            }


            return new ViewModel(array('form'=>$form, 'tipo'=>1, 'persona'=>$person));

        }
        else
            return $this->redirect()->toRoute('persona', array('action'=>'index'));


    }

    public function reportecvAction()
    {
        date_default_timezone_set("America/Lima");
        $id= $this->params()->fromRoute('id', 0);
        $persona=$this->getPersonaTable()->getPersona($id);
        $eventos=$this->getEventoTable()->getEventoPersona($id);
        $trabajos=$this->getTrabajoTable()->getTrabajosPersona($id,"1900-01-01 00:00:00",date("Y-m-d H:i:s"));

        $pdf = new PdfModel();
        // set filename
        $pdf->setOption('filename', 'CV-'.$persona->nombre."-".$persona->apellidopaterno."-".$persona->apellidomaterno.'.pdf');

        // Defaults to "8x11"
        $pdf->setOption('paperSize', 'a4');

        // paper orientation
        $pdf->setOption('paperOrientation', 'portrait');
        $pdf->setVariables(

            array(
                'nombre'=>$persona->nombre." ".$persona->apellidopaterno." ".$persona->apellidomaterno,
                'trabajos'=>$trabajos,
                'eventos'=>$eventos,
                )
        );

        return $pdf;
    }
    private function getBusTable()
    {
        if (!$this->bustable) {
            $sm = $this->getServiceLocator();
            $this->bustable = $sm->get(
                'Bus\Model\BusTable'
            );
        }

        return $this->bustable;
    }
    private function getClienteTable()
    {
        if (!$this->clientetable) {
            $sm = $this->getServiceLocator();
            $this->clientetable = $sm->get(
                'Cliente\Model\ClienteTable'
            );
        }

        return $this->clientetable;
    }

    private function getHabitacionTable()
    {
        if (!$this->habitaciontable) {
            $sm = $this->getServiceLocator();
            $this->habitaciontable = $sm->get(
                'Habitacion\Model\HabitacionTable'
            );
        }

        return $this->habitaciontable;
    }

    private function getMultimediaTable()
    {
        if (!$this->multimediatable) {
            $sm = $this->getServiceLocator();
            $this->multimediatable = $sm->get(
                'Habitacion\Model\MultimediaTable'
            );
        }

        return $this->multimediatable;
    }


}
