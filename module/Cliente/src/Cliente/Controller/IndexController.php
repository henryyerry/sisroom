<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Cliente\Controller;

use Bus\Form\BusForm;
use Bus\Model\Asiento;
use Bus\Model\Bus;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{

    public $dbAdapter;
    public $clientetable;
    public $asientotable;

    public function indexAction()
    {

        return new ViewModel(array(
            "buses",
        ));
    }
    public function disenioAction()
    {
        $id = (int)$this->params()->fromRoute('id', 0);
        if ($this->request->isXmlHttpRequest()) {
            $o = (int)$this->request->getPost('o');
            $idasiento = (int)$this->request->getPost('id');
            switch ($o) {

                case 3:
                    $asiento=$this->getAsientoTable()->getAsiento($idasiento);
                    $asiento->estado='1';
                    $id= $this->getAsientoTable()->saveAsiento($asiento);
                    return new JsonModel(array($id));
                    break;
                case 2:
                    $asiento=$this->getAsientoTable()->getAsiento($idasiento);
                    $asiento->estado='3';
                    $id= $this->getAsientoTable()->saveAsiento($asiento);
                    break;
                case 1:
                    $asiento=$this->getAsientoTable()->getAsiento($idasiento);
                    $asiento->estado='2';
                    $id= $this->getAsientoTable()->saveAsiento($asiento);
                    return new JsonModel(array($id));
                    break;
            }

        }
        $bus= $this->getBusTable()->getBus($id);
        $modulo=$bus->nasientos%4;
        $filas=(int)($bus->nasientos/4);
        if($modulo!=0)
        {
            $filas=$filas+1;

        }
        $acu=0;
        if($bus->estado!=2)
        {
            $asientos=[];
            for($j=1; $j<=5; $j++)
            {
                for($i=1; $i<=$filas; $i++)
                {
                    if($acu<$bus->nasientos)
                    {
                        if($j!=3)
                        {

                            $acu++;
                            $asiento= new Asiento();
                            $asiento->numero=$acu;
                            $asiento->estado=1;
                            $asiento->detalle='';
                            $asiento->columna=$j;
                            $asiento->fila=$i;
                            $asiento->idbus=$bus->idbus;
                            if($j==1||$j==4)
                            {
                                $asiento->tipo=1;
                            }
                            else
                                $asiento->tipo=2;
                            $this->getAsientoTable()->saveAsiento($asiento);
                            $asientos[$acu]=$asiento;
                        }

                    }
                }
            }
            $bus->estado=2;
            $this->getBusTable()->saveBus($bus);

        }

        $asientos= $this->getAsientoTable()->getAsientosBus($bus->idbus);
        return new ViewModel(array(
            "asientos"=>$asientos,'filas'=>$filas, 'bus'=>$bus
        ));


    }
    public function addAction()
    {
        $idlogeado=1;//TODO: CAMBIAR A ID LOGIN
        if ($this->request->isXmlHttpRequest()) {
            $o = (int)$this->request->getPost('o');
            $idpro = (int)$this->request->getPost('id');
            switch ($o) {
                case 2:
                    $unidades= $this->getUnindadTable()->getUnidadDistrito($idpro);
                    return new JsonModel($unidades);
                    break;
                case 1:
                    $unidades= $this->getDistritoTable()->getDistritoProvincia($idpro);
                    return new JsonModel($unidades);
                    break;
            }

        }
        $cliente=$this->getClienteTable()->getCliente($idlogeado);


        return new ViewModel(array($cliente));
    }

    public function editAction()
    {
        $id = (int)$this->params()->fromRoute('id', 0);

        if($id)
        {
            $form= new PersonasForm();
            $person=$this->getPersonaTable()->getPersona($id);
            $foto1=$person->fotodni;
            $foto2=$person->fotopersonal;
            $unidades=$this->getUnindadTable()->getUnidadDistrito(1);
            $unidad = array();
            foreach ($unidades as $value) {
                $unidad[$value['idunidad']] = $value['nombre'];
            }
            $form->get('idunidad')->setAttribute('options', $unidad);
            $form->bind($person);
            $request = $this->getRequest();
            if ($request->isPost()) {
                $form->setInputFilter($person->getInputFilter());

                $post = array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                );
                $form->setData($post);
                if ($form->isValid()) {
                    $person->idpersona=$id;
                    $data = $form->getData();

                    $path = $data->fotodni;
                    $sinpunto=substr($path['tmp_name'], -(strlen($path['tmp_name'])-1));
                    $pathfin=str_replace("\\","/",$sinpunto);
                    $person->fotodni=$pathfin;
                    $path = $data->fotopersonal;
                    $sinpuntos=substr($path['tmp_name'], -(strlen($path['tmp_name'])-1));
                    $pathfins=str_replace("\\","/",$sinpuntos);
                    $person->fotopersonal=$pathfins;

                    if($person->fotodni==""){
                        $person->fotodni=$foto1;
                    }
                    if($person->fotopersonal=="")
                    {
                        $person->fotopersonal=$foto2;
                    }
                    $this->getPersonaTable()->savePersona($person);

                    return $this->redirect()->toRoute(
                        'persona',
                        array(
                            'action' => 'index',
                        )
                    );
                }

            }


            return new ViewModel(array('form'=>$form, 'tipo'=>1, 'persona'=>$person));

        }
        else
            return $this->redirect()->toRoute('persona', array('action'=>'index'));


    }

    public function reportecvAction()
    {
        date_default_timezone_set("America/Lima");
        $id= $this->params()->fromRoute('id', 0);
        $persona=$this->getPersonaTable()->getPersona($id);
        $eventos=$this->getEventoTable()->getEventoPersona($id);
        $trabajos=$this->getTrabajoTable()->getTrabajosPersona($id,"1900-01-01 00:00:00",date("Y-m-d H:i:s"));

        $pdf = new PdfModel();
        // set filename
        $pdf->setOption('filename', 'CV-'.$persona->nombre."-".$persona->apellidopaterno."-".$persona->apellidomaterno.'.pdf');

        // Defaults to "8x11"
        $pdf->setOption('paperSize', 'a4');

        // paper orientation
        $pdf->setOption('paperOrientation', 'portrait');
        $pdf->setVariables(

            array(
                'nombre'=>$persona->nombre." ".$persona->apellidopaterno." ".$persona->apellidomaterno,
                'trabajos'=>$trabajos,
                'eventos'=>$eventos,
                )
        );

        return $pdf;
    }
    private function getClienteTable()
    {
        if (!$this->clientetable) {
            $sm = $this->getServiceLocator();
            $this->clientetable = $sm->get(
                'Cliente\Model\ClienteTable'
            );
        }

        return $this->clientetable;
    }

    private function getAsientoTable()
    {
        if (!$this->asientotable) {
            $sm = $this->getServiceLocator();
            $this->asientotable = $sm->get(
                'Bus\Model\AsientoTable'
            );
        }

        return $this->asientotable;
    }


}
