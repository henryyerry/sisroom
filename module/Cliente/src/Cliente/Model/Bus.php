<?php
namespace Bus\Model;

use Zend\Form\Element;
use Zend\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Authentication\Validator;

class Bus implements InputFilterAwareInterface
{
	public $idbus;
    public $nombretitular;
    public $nplaca;
    public $npoliza;
    public $ntarjetahabilitacion;
	public $ninspecciontecnica;
    public $categoria;
    public $marca;
    public $modelo;
    public $color;
    public $nmotor;
    public $nserie;
    public $aniofabricacion;
    public $nasientos;
    public $pesobruto;
    public $pesoneto;
    public $cargautil;
    public $altura;
    public $ancho;
    public $estado;
    public $inputFilter;

    public function exchangeArray($data)
    {
	    $this->idbus = (!empty($data['idbus']))
		    ? $data['idbus'] : null;
        $this->nombretitular = (!empty($data['nombretitular']))
            ? $data['nombretitular'] : null;
	    $this->nplaca = (!empty($data['nplaca']))
		    ? $data['nplaca'] : 0;
	    $this->npoliza = (!empty($data['npoliza']))
		    ? $data['npoliza'] : 0;
	    $this->ntarjetahabilitacion = (!empty($data['ntarjetahabilitacion']))
		    ? $data['ntarjetahabilitacion'] : 0;
	    $this->ninspecciontecnica = (!empty($data['ninspecciontecnica']))
		    ? $data['ninspecciontecnica'] : 0;
	    $this->categoria = (!empty($data['categoria']))
		    ? $data['categoria'] : 1;
	    $this->marca = (!empty($data['marca']))
		    ? $data['marca'] : null;
        $this->modelo = (!empty($data['modelo']))
            ? $data['modelo'] : null;
	    $this->color = (!empty($data['color']))
		    ? $data['color'] : null;
		$this->nmotor = (!empty($data['nmotor']))
			? $data['nmotor'] : 0;
		$this->nserie = (!empty($data['nserie']))
			? $data['nserie'] : 0;
		$this->aniofabricacion = (!empty($data['aniofabricacion']))
			? $data['aniofabricacion'] : null;
		$this->nasientos = (!empty($data['nasientos']))
			? $data['nasientos'] : 0;
		$this->pesobruto = (!empty($data['pesobruto']))
			? $data['pesobruto'] : 0;
		$this->pesoneto = (!empty($data['pesoneto']))
			? $data['pesoneto'] : 0;
		$this->cargautil = (!empty($data['cargautil']))
			? $data['cargautil'] : 0;
		$this->altura = (!empty($data['altura']))
			? $data['altura'] : 0;
		$this->ancho = (!empty($data['ancho']))
			? $data['ancho'] : 0;
        $this->estado = (!empty($data['estado']))
            ? $data['estado'] : '1';

    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter\InputFilter();

            $inputFilter->add(
                array(
                    'name'     => 'idbus',
                    'required' => false,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                )
            );
	        $inputFilter->add(
		        array(
			        'name'     => 'nombretitular',
			        'required' => true,
			        'filters'    => array(
				        array('name' => 'StripTags'),
				        array('name' => 'StringTrim'),
			        ),
		        )
	        );


            $inputFilter->add(
                array(
                    'name'       => 'nplaca',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese número placa"
                                ),
                            )
                        ),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'       => 'npoliza',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese número poliza"
                                ),
                            )
                        ),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'       => 'ntarjetahabilitacion',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese nº tarje habilitaci"
                                ),
                            )
                        ),
                    ),
                )
            );

            $inputFilter->add(
                array(
                    'name'       => 'ninspeciontecnica',
                    'required'   => false,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese nº"
                                ),
                            )
                        ),
                    ),
                )
            );

            $inputFilter->add(
                array(
                    'name'       => 'categoria',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Seleccione un categoria"
                                ),
                            )
                        ),
                    ),
                )
            );
	        $inputFilter->add(
		        array(
			        'name'       => 'marca',
			        'required'   => true,
			        'filters'    => array(
				        array('name' => 'StripTags'),
				        array('name' => 'StringTrim'),
			        ),
			        'validators' => array(
				        array(
					        'name'    => 'StringLength',
					        'options' => array(
						        'encoding' => 'UTF-8',
						        'min'      => 1,
						        'max'      => 50,
						        'messages' => array(
							        \Zend\Validator\StringLength::TOO_SHORT => 'Marca de tener al menos un carácter',
							        \Zend\Validator\StringLength::TOO_LONG  => 'Marca no puede superar os 50 caracteres',
						        ),
					        ),
				        ),
				        array(
					        'name'    => 'NotEmpty',
					        'options' => array(
						        'messages' => array(
							        \Zend\Validator\NotEmpty::IS_EMPTY => "Ingresar una marca"
						        ),
					        )
				        ),
			        ),
		        )
	        );
	        $inputFilter->add(
		        array(
			        'name'       => 'modelo',
			        'required'   => true,
			        'filters'    => array(
				        array('name' => 'StripTags'),
				        array('name' => 'StringTrim'),
			        ),
			        'validators' => array(
				        array(
					        'name'    => 'StringLength',
					        'options' => array(
						        'encoding' => 'UTF-8',
						        'min'      => 1,
						        'max'      => 50,
						        'messages' => array(
							        \Zend\Validator\StringLength::TOO_SHORT => 'Modelo de tener al menos un carácter',
							        \Zend\Validator\StringLength::TOO_LONG  => 'Modelo no puede superar os 50 caracteres',
						        ),
					        ),
				        ),
				        array(
					        'name'    => 'NotEmpty',
					        'options' => array(
						        'messages' => array(
							        \Zend\Validator\NotEmpty::IS_EMPTY => "Ingresar una modelo"
						        ),
					        )
				        ),
			        ),
		        )
	        );
	        $inputFilter->add(
		        array(
			        'name'       => 'color',
			        'required'   => true,
			        'filters'    => array(
				        array('name' => 'StripTags'),
				        array('name' => 'StringTrim'),
			        ),
			        'validators' => array(
				        array(
					        'name'    => 'StringLength',
					        'options' => array(
						        'encoding' => 'UTF-8',
						        'min'      => 1,
						        'max'      => 50,
						        'messages' => array(
							        \Zend\Validator\StringLength::TOO_SHORT => 'Color de tener al menos un carácter',
							        \Zend\Validator\StringLength::TOO_LONG  => 'Color no puede superar os 50 caracteres',
						        ),
					        ),
				        ),
				        array(
					        'name'    => 'NotEmpty',
					        'options' => array(
						        'messages' => array(
							        \Zend\Validator\NotEmpty::IS_EMPTY => "Ingresar una color"
						        ),
					        )
				        ),
			        ),
		        )
	        );
            $inputFilter->add(
                array(
                    'name'       => 'nmotor',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese un número de motor"
                                ),
                            )
                        ),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'       => 'nserie',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese un número de serie"
                                ),
                            )
                        ),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'       => 'aniofabricacion',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Seleccione el año de fabricación"
                                ),
                            )
                        ),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'       => 'nasientos',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese la cantidad de asientos"
                                ),
                            )
                        ),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'       => 'pesobruto',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese el peso bruto"
                                ),
                            )
                        ),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'       => 'pesoneto',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese peso neto"
                                ),
                            )
                        ),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'       => 'cargautil',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese carga util"
                                ),
                            )
                        ),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'       => 'altura',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese altura"
                                ),
                            )
                        ),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'       => 'ancho',
                    'required'   => true,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Ingrese su ancho"
                                ),
                            )
                        ),
                    ),
                )
            );
            $inputFilter->add(
                array(
                    'name'       => 'estado',
                    'required'   => false,
                    'filters'    => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    \Zend\Validator\NotEmpty::IS_EMPTY => "Seleccione un estado"
                                ),
                            )
                        ),
                    ),
                )
            );

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

}